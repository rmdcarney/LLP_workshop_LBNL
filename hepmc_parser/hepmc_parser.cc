// -*- C++ -*-
//
// This file is part of HepMC
// Copyright (C) 2014 The HepMC collaboration (see AUTHORS for details)
//
// This file reads in a hepmc file, performs an operation on particles with specific properties, and writes out new hepmc file.
// In this example, the code class "trigstubs.h" checks whether or not a track is reconstructed by the CMS track trigger. It stores then stores this information in the status code of the particles in the outgoing hepmc file.
// writen by Simon 06/29/2018.


#include "HepMC/IO_GenEvent.h"
#include "HepMC/GenEvent.h"
#include "trigstubs.h"
#include <stdlib.h>

// replace this class with your own
class AddTrackEff {
public:
    
    /// check this event and perform an operation on it
    HepMC::GenEvent operator()( const HepMC::GenEvent* evt ) { 
        
    trigstubs ts;  // create track trigger object
    HepMC::FourVector mom;
    HepMC::FourVector vert;
    int charge;    
    bool trackreco;
    
    // loop over all particles in the event    
	for ( HepMC::GenEvent::particle_const_iterator p 
		  = evt->particles_begin(); p != evt->particles_end(); ++p ){
	    if ( makesTrack((*p)->pdg_id()) && (*p)->status()==1 ) {
        
        mom=(*p)->momentum();//momentum
        vert=(*p)->production_vertex()->position(); //vertex location    
        charge = (((*p)->pdg_id()) > 0) - (((*p)->pdg_id())< 0);//sign function
        
        // call track trigger function, returns bool    
        trackreco=ts.stubmap(mom.px(),mom.py(),vert.x(),vert.x(),charge); //std::cout << trackreco <<"\n";
        // store the result in the status code of the particle    
        if(trackreco){(*p)->set_status(21);
                     }else{
                    (*p)->set_status(11);
                }
	    } 
	}
	return 0;
    }
    
private:    
    //selects which particles are to be considered, atm electrons and muons
    bool makesTrack(int pdgcode){
        if(abs(pdgcode)==11 || abs(pdgcode)==13){
            return 1;}
        return 0;
    }
    
};



int main(int argc, char *argv[]) { 
   
    if(!(argc==3)){
    std::cout << "I need inputfile name and outputfile name";
    return 0;
  }
    
    { // begin scope of ascii_in and ascii_out
           
	HepMC::IO_GenEvent ascii_in(argv[1],std::ios::in);
	// declare another IO_GenEvent for writing out the good events
	HepMC::IO_GenEvent ascii_out(argv[2],std::ios::out);
	// declare an instance of the event selection predicate
	AddTrackEff add_track_eff;
	//........................................EVENT LOOP
	int icount=0;
	int num_good_events=0;
	HepMC::GenEvent* evt = ascii_in.read_next_event();
	while ( evt ) {
	    icount++;
	    if ( icount%50==1 ) std::cout << "Processing Event Number " << icount
					  << " its # " << evt->event_number() 
					  << std::endl;
	    add_track_eff(evt);
		ascii_out << evt;
		++num_good_events;
	    
        delete evt;
	    ascii_in >> evt;
	}
	//........................................PRINT RESULT
	std::cout << num_good_events << " out of " << icount 
		  << " processed events passed the cuts. Finished." << std::endl;
    } // end scope of ascii_in and ascii_out
    return 0;
} 



