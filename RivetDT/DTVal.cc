// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/RivetAIDA.hh"
#include "Rivet/RivetHepMC.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
//#include "Rivet/Projections/DressedLeptons.hh" //--- Rivet 2.X
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/LeptonClusters.hh"
#include "HepMC/GenVertex.h"

using namespace std;

namespace Rivet {


  class DTVal : public Analysis {
  private:

    // Useful enums, constants, etc..
    
    enum PhaseSpaceSelections {
      ps_PreFilter, ///< number of starting events
      ps_All, ///< no selections (after filters)
      ps_JetPt, ///< number of jets
      ps_NCuts
    };

    static bool ptbare_comp(const ClusteredLepton p1,const ClusteredLepton p2)
    {
      return (p1.constituentLepton().momentum().pT() > p2.constituentLepton().momentum().pT());
    }


  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    DTVal()
      : Analysis("DTVal")
    {
      idxEvent=-1;
      setNeedsCrossSection(true);
    }    

    //@}

  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      //init cut values
      _cutValues[ps_PreFilter] = -999.0; // not set
      _cutValues[ps_All] = -999.0; // not set
      _cutValues[ps_JetPt] = 50.0;

      //reset counters
      for (int ps = ps_PreFilter; ps <= ps_NCuts; ps++) {
        PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);
        _processedEvents[idx]=0;
        _fiducialWeights[idx]=0.0;
        _fiducialWeights2[idx]=0.0;
      }

      // Initialise and register projections here
      //Jets, anti-kt 0.4
      FinalState fs(-7.0, 7.0, 0.*GeV);
      addProjection(fs, "fs");
      VetoedFinalState fsJets(fs); //final state for jet finding: veto leptons and neutrinos
      fsJets.vetoNeutrinos();
      //fsJets.addVetoPairDetail(ELECTRON, 10.*GeV, numeric_limits<double>::max());
      fsJets.addVetoPairDetail(MUON, 0.*GeV, numeric_limits<double>::max());      
      fsJets.addVetoPairDetail(1000024,0.*GeV, numeric_limits<double>::max());      
      fsJets.addVetoPairDetail(-1000024,0.*GeV, numeric_limits<double>::max());      
      //fsJets.addVetoPairDetail(TAU, 10.*GeV, numeric_limits<double>::max());
      addProjection(FastJets(fsJets, FastJets::ANTIKT, 0.4), "Jets");
      
     
      IdentifiedFinalState charginosfs = IdentifiedFinalState(FinalState(-1000., 1000., 0.0));
      charginosfs.acceptIdPair(1000024);
      charginosfs.acceptIdPair(-1000024);
      addProjection(charginosfs, "charginos");
      
      /*      IdentifiedFinalState charginosus = IdentifiedFinalState(UnstableFinalState(-1000., 1000., 0.0));
      charginosus.acceptIdPair(1000024);
      charginosus.acceptIdPair(-1000024);
      addProjection(charginosus, "charginos_unstable"); */
      

      // --- Book histograms
      _h_NJets = bookHistogram1D("NJets", 10, 0., 10.);
      _h_jet1_pt = bookHistogram1D("jet1_pt", 200, 0., 1000.);
      _h_jet1_eta = bookHistogram1D("jet1_eta", 120, -6., 6.);
      _h_chargino_pt = bookHistogram1D("chargino_pt", 200, 0, 1000.);
      _h_chargino_m = bookHistogram1D("chargino_m", 200, 0, 1000.);
      _h_NCharginos = bookHistogram1D("NCharginos", 5, 0., 5.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      idxEvent++;
      MSG_DEBUG("Processing " << idxEvent << " event.");
      const double weight = event.weight();

      for (int ps = ps_PreFilter; ps <= ps_NCuts; ps++) {
        PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);
        _passCuts[idx]=false;
      }

      cutFlow(ps_PreFilter, weight);
      _passCuts[ps_PreFilter] = true;

      const FastJets& jetsProjection = applyProjection<FastJets>(event, "Jets");
      const Jets& jets = jetsProjection.jetsByPt(20.*GeV); //down to 20 GeV

      const ParticleVector& charginos = (applyProjection<IdentifiedFinalState>(event, "charginos")).particlesByPt();
      //      const ParticleVector& charginos_unstable = (applyProjection<IdentifiedFinalState>(event, "charginos_unstable")).particlesByPt();

      //-- Evaluate selections cuts
      MSG_DEBUG("Evaluating selection cuts");
      _passCuts[ps_All] = true;

      //jets
      MSG_DEBUG("Jets");
      foreach (const Jet& jet, jets) {
        //check eta,pT acceptance 
	if (jet.momentum().pT() < _cutValues[ps_JetPt]) continue;
	_passCuts[ps_JetPt] = true;	
      }

      int nCharginos = 0;
      foreach (const Particle &ch, charginos) {
	_h_chargino_m->fill(ch.mass()*GeV, weight);
	_h_chargino_pt->fill(ch.momentum().pT()*GeV,weight);
	nCharginos++;
      }

      /*      foreach (const Particle &ch, charginos_unstable) {
		float lifetime = 0;
		GenVertex * vertex = ch.genParticle().end_vertex();
		if (vertex) lifetime = vertex->position().t();
		//		_h_chargino_lifetime->fill(lifetime,weight);
		MSG_INFO(" lifetime " << lifetime);
		if (vertex) MSG_INFO(" position x " << vertex->position().x());
		MSG_INFO(" status " << ch.genParticle().status()); // 1 = undecayed, 2 = decayed
		} */

      //=== Apply selections
      cutFlow(ps_All, weight);
      if (!_passCuts[ps_JetPt]) vetoEvent; 
      cutFlow(ps_JetPt, weight);

      //loose fiducial selection
      cutFlow(ps_NCuts, weight); //for easy counting

      //== Fill plots after selections
      _h_NJets->fill(jets.size(), weight);
      _h_jet1_pt->fill(jets[0].momentum().pT()*GeV, weight);
      _h_jet1_eta->fill(jets[0].momentum().eta(), weight);
      _h_NCharginos->fill(nCharginos,weight);	
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_NJets, crossSection()/sumOfWeights());
      scale(_h_jet1_pt, crossSection()/sumOfWeights());
      scale(_h_jet1_eta, crossSection()/sumOfWeights());
      //      scale(_h_chargino_pt, crossSection()/sumOfWeights());
      // scale(_h_chargino_m, crossSection()/sumOfWeights());
      // scale(_h_chargino_lifetime, crossSection()/sumOfWeights());

      //Print counters
      MSG_INFO("Printing cutflow:");
      for (int ps = ps_PreFilter; ps < ps_NCuts; ps++) {
        PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);
        string selName = getCutName(idx);
        double eff, eff_err;
        PhaseSpaceSelections denIdx = ps_All;
        double xsec = crossSection();//in pb
        xsec = xsec * 1E3; //move to fb
        if (ps == ps_PreFilter || ps == ps_All) {
          denIdx = ps_PreFilter;
        } else {
          //efficiency defined wrt filtered, correct xsec
          xsec = xsec * _fiducialWeights[ps_All] / _fiducialWeights[ps_PreFilter]; 
        }
        calculateEfficiency(eff, eff_err, 
                            _fiducialWeights[idx], _fiducialWeights[denIdx],
                            _fiducialWeights2[idx], _fiducialWeights2[denIdx]);
        MSG_INFO(" " << selName);
        if (selName.find("disabled") == std::string::npos) {
          MSG_INFO("   Raw events: " << _processedEvents[idx]);
          MSG_INFO("   Weighted events: "<< _fiducialWeights[idx]);
          MSG_INFO("   Efficiency = " << eff << " +/- " << eff_err);        
          MSG_INFO("   x-sec = " << xsec * eff << " +/- " << xsec*eff_err << " fb");
        }
      }
    } 

    //@}

    /// @name Analysis helpers
    //@{

    string getCutName(PhaseSpaceSelections ps) {
      ostringstream outStr; //string stream, if needed
      switch (ps) {
      case ps_PreFilter:
        return string("Pre-Filter");
      case ps_All:
        return string("All");
      case ps_JetPt: 
	{
	  outStr << ">= 1 Jet with pT >= ";
	  outStr << _cutValues[ps_JetPt];
	  return outStr.str();
	}
      }
      return string("UNKNOWN");
    }

    ///Increment cutflow
    void cutFlow(PhaseSpaceSelections ps, double weight) {
      MSG_DEBUG("Passing cut: " << getCutName(ps));
      _processedEvents[ps]++;
      _fiducialWeights[ps]+= weight;
      _fiducialWeights2[ps]+= weight*weight;      
    }

    ///Check all cuts but one
    bool checkAllBut(PhaseSpaceSelections ps) {
      bool ret(true);
      for (int i = ps_PreFilter; i <= ps_NCuts; i++) {
        if (ps == i) continue; //do not check this
        if (!_passCuts[i]) {
          ret=false;
          break;
        }
      }
      return ret;
    }

    ///Print cut pass (for debug)
    void printPassCuts() {
      for (int i = ps_PreFilter; i <= ps_NCuts; i++) {
        PhaseSpaceSelections ps = static_cast<PhaseSpaceSelections>(i);
        MSG_DEBUG(getCutName(ps) << ": " << _passCuts[ps]);
      }
    }

    /** Calculate efficiency and errors.
     * @param eff return efficiency value
     * @param eff_err return efficiency error
     * @param w_sel sum of weights that pass selections
     * @param w_all sum of weight of all events
     * @param w2_sel sum of squared weights that pass selections
     * @param w2_all sum of squared weights of all events
     */
    void calculateEfficiency(double &eff, double &eff_err,
                             double w_sel, double w_all,
                             double w2_sel, double w2_all) {
      if (w_all != 0)
        eff = w_sel / w_all;
      else {
        MSG_INFO("Error calculating average. Zero denominator: " << w_sel << " / " << w_all << ". Forcing -1.");
        eff = eff_err = -1;
      }

      double tmp_w2_m(0.0); //sum of squared weights for events not passing selection
      double tmp_w_m(0.0); //sum of weights for events not passing selection
      tmp_w_m = w_all - w_sel;
      tmp_w2_m = w2_all - w2_sel;
      eff_err = sqrt(w2_sel*tmp_w_m*tmp_w_m + tmp_w2_m*w_sel*w_sel);
      eff_err = eff_err / (w_all*w_all);
            
    }

    void myNormalize(AIDA::IHistogram1D *h) {
      //takes into account bin widths
      normalize(h, h->axis().binWidth(0)); //assume equally-spaced bins
    }


    //@}

  private:

    // cut values
    map<PhaseSpaceSelections, float> _cutValues;

    /// event-level cuts bookeeping
    bool _passCuts[ps_NCuts+1];

    // Data members like post-cuts event weight counters go here    
    map<PhaseSpaceSelections, int> _processedEvents;
    map<PhaseSpaceSelections, double> _fiducialWeights;
    map<PhaseSpaceSelections, double> _fiducialWeights2;

  private:

    /// @name Histograms
    //@{

    //histograms after selection
    AIDA::IHistogram1D *_h_jet1_pt;
    AIDA::IHistogram1D *_h_jet1_eta;
    AIDA::IHistogram1D *_h_NJets;

    AIDA::IHistogram1D *_h_chargino_pt;
    AIDA::IHistogram1D *_h_chargino_m;
    AIDA::IHistogram1D *_h_NCharginos;

    //@}

    int idxEvent;

  };



  // This global object acts as a hook for the plugin system
  AnalysisBuilder<DTVal> plugin_DTVal;


}
