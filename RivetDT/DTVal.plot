### Jet plots
# BEGIN PLOT /DTVal/jet1_pt
Title=1st jet $p_T$
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
Rebin=5
# END PLOT
# BEGIN PLOT /DTVal/jet1_eta
Title=1st jet $\eta$
XLabel=$\eta$ (GeV)
YLabel=Entries
LogY=0
Rebin=6
# END PLOT
# BEGIN PLOT /DTVal/NJets
Title=Jet Multiplicity
XLabel=Number of Jets
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /DTVal/chargino_pt
Title=Chargino pT
XLabel=pT [GeV]
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /DTVal/chargino_m
Title=Chargino Mass
XLabel=Mass [GeV]
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /DTVal/NCharginos
Title=Chargino Multiplicity
XLabel=Number of Charginos per Event
YLabel=Entries
LogY=0
# END PLOT
